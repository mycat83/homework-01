# Project Name
Abstract your project in few lines.
see [project sample page](project link)

## Documentation
### Installation
To install,
`$ pip install sesame`
and run `$ python open_sesame.py`

### Supported Python versions `>=3.6`
### More Information
- [API docs]()
- [Official website]()

### Contributing
Please see [CONTRIBUTING.md]()

### License
Sesame is Free software, and may be redistributed under the terms of specified in the [LICENSE]() file.
